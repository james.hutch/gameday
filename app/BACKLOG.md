### Cosmetic

- Fix all CSS look and feel, centre icons on shop, cart and index pages [Ref](https://picnicss.com/).

### Users

- Implement user login/logout [Ref](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-v-user-logins). The basic structure is there but will need to create a way of storing users and then per user logic.

### Database

- Implement backend database for storing user information, optionally extend to store product information and cart for a logged in user [Ref](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database).

### Sessions

- Extend session store to use intermediate storage layer -- this will be required to scale front end.


