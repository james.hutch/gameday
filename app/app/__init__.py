from flask import Flask, session
from flask_login import LoginManager
from flask_session import Session
from flask_bcrypt import Bcrypt

from config import Config

app = Flask(__name__)
app.config.from_object(Config)
Session(app)
bcrypt = Bcrypt(app)

from app import routes
