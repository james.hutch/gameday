from flask_login import UserMixin
from app import bcrypt

from collections import namedtuple

class UserModel(UserMixin):

    def set_password(self, password):
        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

Product = namedtuple('Product', [
    "name",
    'description',
    'icon',
    ])

products = [
        Product("diplodocus", "", "icons/diplodocus.svg"),
        Product("pterodactyl", "", "icons/pterodactyl.svg"),
        Product("stegosaurus", "", "icons/stegosaurus.svg"),
        Product("triceratops", "", "icons/triceratops.svg"),
        Product("tyrannosaurus","", "icons/tyrannosaurus.svg"),
        ]
