from flask import render_template, flash, session, redirect, url_for
from flask_login import current_user, login_user

from app import app
from app.forms import LoginForm, RegistrationForm
from app.model import UserModel, Product, products

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET','POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash("Login requested for user {}, remember_me={}".format(
            form.username.data, form.remember_me.data))
        return redirect(url_for('index'))

    return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/register', methods=['GET','POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        return redirect(url_for('index'))
    return render_template('register.html', title="Register", form=form)    

@app.route('/shop')
def shop():
    return render_template('shop.html', products=products)

def get_cart():
    # Return cart from session or create empty array of 0s
    return session.get('cart', [0]*len(products))

def save_cart(cart):
    session['cart'] = cart

@app.route('/cart')
def cart():
    cart = get_cart()
    return render_template('cart.html', cart=cart, products=products, empty_cart=[0]*len(products))

@app.route('/add_item/<int:item>')
def add_item(item):
    if item in range(len(products)):
        cart = get_cart()
        cart[item] = cart[item] + 1
        save_cart(cart)
    return redirect(url_for('cart'))

@app.route('/remove_item/<int:item>')
def remove_item(item):
    if item in range(len(products)):
        cart = get_cart()
        if cart[item] > 0:
            cart[item] = cart[item] - 1
        save_cart(cart)
    return redirect(url_for('cart'))
    
@app.route('/empty')
def empty_cart():
    try:
        session.clear()
        return redirect(url_for('index'))
    except Exception as e:
        print(e)
