### Running the site

You will need to create a virtual environment using the venv package. For ubuntu this will involve first installing `python3-venv` using apt.

Set up a virtual environment and then use `pip` to install all the packages in `requirements.txt`.

You may want to modify .flaskenv to add a SECRET_KEY variable that will be used to encode the session cookies.

Finally run the site using `flask run` -- you can connect to it on http://localhost:5000/

### Libraries

The following libraries are used in this code:

- [Flask]() - top level web routing framework
- [Flask Session](https://pythonhosted.org/Flask-Session/) for server side sessions.
- [Flask Bcrypt](https://flask-bcrypt.readthedocs.io/en/latest/) for password encryption.
- [Flask WTForms](https://flask-wtf.readthedocs.io/en/stable/) for creating HTML forms.
- [python-dotenv](https://github.com/theskumar/python-dotenv) for storing environment constants in .env files.

The following libraries will probably be required in order to extend the functionality of this application:

- [SQLAlchemy](https://docs.sqlalchemy.org/en/13/) for database access.
- [redis-py](https://github.com/andymccurdy/redis-py) for storing sessions when you have multiple frontends.
