import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or "zu2iet7R Oht2fihe neeY6vai"
    SESSION_TYPE = "filesystem"
    TEMPLATES_AUTO_RELOAD = os.environ.get('TEMPLATES_AUTO_RELOAD') or False
